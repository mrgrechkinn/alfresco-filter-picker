<import resource="classpath:alfresco/extension/templates/webscripts/ru/mrgrechkinn/alfresco/common/underscore.js">
<import resource="classpath:/alfresco/templates/webscripts/org/alfresco/repository/forms/pickerresults.lib.js">

function findUsers(searchTerm, maxResults, results)
{
   var searchResults = people.getPeople(searchTerm, maxResults, "lastName", true);

   // create person object for each result
   for each(var userRef in searchResults)
   {
      var user = search.findNode(userRef);

      if (logger.isLoggingEnabled())
         logger.log("found user = " + user.properties['cm:userName']);

      // add to results
      results.push(
      {
         item: createPersonResult(user),
         selectable: true
      });
   }
}

function main() {
    var argsSearchTerm = args['searchTerm'],
        argsGroupsTerm = args['group']
        results = [];

    if (logger.isLoggingEnabled()) {
       logger.log("argsSearchTerm = " + argsSearchTerm);
       logger.log("argsGroupsTerm = " + argsGroupsTerm);
    }

    // Find users in group 
    if (argsGroupsTerm) {
        var group = people.getGroup("GROUP_" + argsGroupsTerm);
        if (group) {
            _.chain(people.getMembers(group, true))
             .filter(function(user) {
                 // If search term is empty or equal wildcard then matches everyone
                 if (!argsSearchTerm || "*" == argsSearchTerm) return true;

                 argsSearchTerm = argsSearchTerm.toLowerCase();

                 return (user.properties.firstName.toLowerCase().indexOf(argsSearchTerm) >= 0
                      || user.properties.lastName.toLowerCase().indexOf(argsSearchTerm) >= 0
                      || user.properties.userName.toLowerCase().indexOf(argsSearchTerm) >= 0);
             })
             .each(function(user) {
                 results.push({
                     item: createPersonResult(user),
                     selectable: true
                 });
             });
        }
    } else {
        findUsers(argsSearchTerm, 100, results);
    }
    model.results = results;
}

main();